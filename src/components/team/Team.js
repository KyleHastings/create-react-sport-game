import React from "react";
import Swish from "../../resources/sounds/Swish.wav"
import BackBoard from "../../resources/sounds/BackBoard.wav"
class Team extends React.Component {
  state = {
    shots: 0,
    score: 0,
  };

  shootBall = () => {
    let randomNum = Math.floor(Math.random() * 4 + 1);
    if (randomNum < 3) {
      this.setState({ shots: this.state.shots + 1 });
      this.setState({ score: this.state.score + 1 });
      let audioElement = new Audio(Swish);
      audioElement.play();
    } else {
      this.setState({ shots: this.state.shots + 1 });
      let audioElement = new Audio(BackBoard);
      audioElement.play();
    }
  };

  render() {
    if (this.state.shots !== 0) {
      return (
        <div className="home_container">
          <img className="team_image" src={this.props.imageURL} alt="not found"></img>
          <h1 className="team_name">These are the {this.props.name}!</h1>
          <button onClick={this.shootBall}>Shoot Ball</button>
          <h1 className="shots_counter">Shots Made: {this.state.shots}</h1>
          <h1 className="score_counter">Score: {this.state.score}</h1>
          <h1 className="stat_counter">Shot Percentage: {((this.state.score / this.state.shots) * 100).toFixed(2)}%</h1>
        </div>
      );
    } else {
      return (
        <div className="away_container">
          <img className="team_image" src={this.props.imageURL} alt="not found"></img>
          <h1 className="team_name">These are the {this.props.name}!</h1>
          <button onClick={this.shootBall}>Shoot Ball</button>
          <h1 className="shots_counter">Shots Made: {this.state.shots}</h1>
          <h1 className="score_counter">Score: {this.state.score}</h1>
        </div>
      );
    }
  }
}

export default Team;
