import React from "react";
import Team from "../team/Team";


function Game(props) {
  return (
    <div className="game_container">
      <h1 className="stadium_name">Welcome to the {props.name} stadium!</h1>
      <br></br>
      <Team name={props.team1} imageURL={props.team1IMG} />
      <br></br>
      <Team name={props.team2} imageURL={props.team2IMG} />
    </div>
  );
}

export default Game;
