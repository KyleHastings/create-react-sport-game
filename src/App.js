import React from "react";
//import logo from './logo.svg';
import "./App.css";
import lakers from "./resources/images/lakers.png";
import celtics from "./resources/images/celtics.png";

import Game from "./components/game/Game";

function App() {
  return <Game name="Boston" team1 = "lakers" team1IMG = {lakers}  team2 = "celtics" team2IMG = {celtics} />;
}

export default App;
